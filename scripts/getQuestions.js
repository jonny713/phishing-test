const csv = require('csv-parser')
// const path = require('path');
const fs = require('fs')

function checkFormat(data, properties = []) {
  for (let property of properties) {
    if (typeof data[property] !== 'string') {
      console.log(data[property],property,data)
      return false
    }
  }
  return true
}

/**
 * @summary Достает вопросы из csv-файла и выдает в виде массива структурированных объектов
 * @param {string} path - Путь к csv-файлу
 */
function getQuestions(path) {
  if (!fs.existsSync(path)) {
    return `ФАЙЛА ${path} НЕТ!`
  }

  return new Promise((resolve, reject) => {
    const results = [];
    fs.createReadStream(path)
      .pipe(csv({ separator: ';' }))
      .on('data', (data) => {
        // Выполняем проверку формата
        if (!checkFormat(data, ['question','pathToImage','answers','trueAnswers'])) {
          return resolve(`Отсутствуют нужные столбцы!`)
        }

        // Парсим массивы
        let answers = data.answers.slice(2, -2).split(/',\s?'/)
        // let answers = data.answers.slice(2, -2).split('\',\'')

        let trueAnswers = data.trueAnswers.slice(2, -2).split(/',\s?'/)
        // let trueAnswers = data.trueAnswers.slice(2, -2).split('\',\'')

        results.push({
          question: data.question,
          pathToImage: data.pathToImage,
          answers,
          trueAnswers,
        })
      })
      .on('end', () => {
        // console.log(results)
        resolve(results)
      })
      .on('error', (error) => {
        console.log(error);
        reject(error)
      });
  })
}

module.exports = getQuestions
