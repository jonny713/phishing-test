var express = require('express');
var path = require('path');
var router = express.Router();

const getQuestions = require('./../scripts/getQuestions');
const answersModel = require('./../libs/mongo/db').collection('answers');

/* GET home page. */
router.get('/oprosnik', async function(req, res, next) {
  res.render('index', { title: 'Express'});
});

router.get('/questions', async function(req, res, next) {
  let questions = await getQuestions(path.join(__dirname, '../questions.csv'))
  res.send({questions});
});

router.post('/check-answers', async function(req, res, next) {
  let { code, answerNumbers } = req.body;
  let questions = await getQuestions(path.join(__dirname, '../questions.csv'))
  let mistakes = [];

  for (let i = 0; i < questions.length; i++) {
    //Проверям количесвто ответов пользователя с количеством правильных ответов
     if (questions[i].trueAnswers.length !== answerNumbers[i].length) {
      console.log(questions[i].trueAnswers)

      mistakes.push(`Ошибка в вопросе "${
        questions[i].question
      }"
      Даны ответы "${
        answerNumbers[i].map(number => questions[i].answers[number]).join('", "')
      }"
      Верные ответы "${
        questions[i].answers.join('", "')
      }"`)
      continue
    }


    //Получем из номером ответов текст ответов

    let answers = []
    for (let answerNumber of answerNumbers[i]) {
      answers.push(questions[i].answers[answerNumber])
    };

    //Сверяем ответы

    for (let j = 0; j < questions[i].trueAnswers.length; j++) {
      if (answers[j] !== questions[i].trueAnswers[j]) {
        console.log(answers[j] , questions[i].trueAnswers[j])

        mistakes.push(`Ошибка в вопросе "${
          questions[i].question
        }"
        Даны ответы "${
          answerNumbers[i].map(number => questions[i].answers[number]).join('", "')
        }"
        Верные ответы "${
          questions[i].answers.join('", "')
        }"`)
      }
    }
  }

  // Сохраняем результат в БД
  try {
    const result = await answersModel.insertOne({
      code,
      dateCreate: new Date(),
      mistakes,
    });
    console.log(result);

  } catch (e) {
    console.error('Не удалось сохранить');

  }

  // Отправляем результаты
  res.send({
    success: !mistakes.length,
    // mistakes
  });
});

module.exports = router;
