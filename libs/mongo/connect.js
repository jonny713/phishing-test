const config = require('./../../config');
const { MongoClient } = require("mongodb");

// const uri = `mongodb+srv://${config.mongo.name}:${config.mongo.pass}@${config.mongo.ip}?retryWrites=true&w=majority`;
const uri = `mongodb://${config.mongo.ip}:27017`

const client = new MongoClient(uri);

module.exports = { client }
