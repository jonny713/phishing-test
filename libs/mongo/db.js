const { client } = require('./connect.js');

class Db {
  constructor() {
    this.database = client.db('phishingDb');
  }

  collection(collectionName) {
    return this.database.collection(collectionName);
  }
}

module.exports = new Db()
