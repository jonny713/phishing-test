/**
* Подключаем параметры для http сервера
*/
const {
  PORT
} = process.env


/**
* Проверка наличия необходимых параметров
*/

module.exports = {
  port: PORT
}
