const path = require('path')
require('dotenv').config({ path: path.join(__dirname, '../.env') })

const http = require('./http')
const mongo = require('./mongo')

module.exports = {
  http,
  mongo,
}
