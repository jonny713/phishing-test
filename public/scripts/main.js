document.addEventListener("DOMContentLoaded", async () => {
    // Запрашиваем список вопросов с сервера
    let questions = await getQuestions()
    // Отрисовываем вопросы
    renderQuestions(questions)
});
