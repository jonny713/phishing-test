async function getQuestions () {
    let { questions } = await fetch('/questions').then(async(response) => {
        let json = await response.json()
        return json
    });
    return questions
}

async function checkAnswers (answerNumbers) {
    let {success , mistakes} = await fetch('/check-answers', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
          code: localStorage.getItem('code'),
          answerNumbers
        })
    } ) .then(async(response) => {
        let json = await response.json()
        return json
    });
    return {success , mistakes}
}
