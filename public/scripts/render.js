function renderCheckBox (parent, questionNumber, answerNumber) {
    let checkBox = document.createElement('input');
    checkBox.type = 'checkbox';
    checkBox.classList.add('answer');
    checkBox.dataset.questionNumber = questionNumber;
    checkBox.dataset.answerNumber = answerNumber;
    parent.append(checkBox);

}

function changeResult (isSuccess, text) {
    let label = document.querySelector('#result')
    label.className = isSuccess ? 'green' : 'red';
    if (text) {
        label.textContent = text
    } else {
        label.textContent = isSuccess ? 'Успешно' : 'Неправильно';
    }
}

function renderResult (parent, text, isSuccess) {
    let label = document.createElement('div');
    label.id = 'result'
    // label.className = isSuccess ? 'green' : 'red';
    // label.textContent = text;
    parent.append(label);
    // parent.append(document.createElement('br'))

}

function renderLabel (parent, text, isBold) {
    let label = document.createElement(isBold ? 'b' : 'label');
    label.textContent = text;
    parent.append(label);
    parent.append(document.createElement('br'))
}

function renderImg (parent, pathToImage) {
    if (!pathToImage) {
        return
    }
    let img = document.createElement('img');
    img.src = '/img/' + pathToImage;
    img.className = 'test-question-img';
    parent.append(img);
    parent.append(document.createElement('br'))
}

async function onButtonClick (parent) {
    let answers = document.querySelectorAll('.answer');
    let result = [];
    for(let answer of answers) {
        result[answer.dataset.questionNumber] = result[answer.dataset.questionNumber] || [];
        // Если чекбокс отмечен добовляем номер ответа в результаты
        if (answer.checked) {
            result[answer.dataset.questionNumber].push(answer.dataset.answerNumber);
        }
    }
    // Проверка что чекбоксы проставлены по всем вопросам
    if (result.some ((value) => !value.length)) {
        changeResult(false, 'Не все заполнено')
        return
    }
    // Отправляем на сервер
    let {success , mistakes} = await checkAnswers(result);
    console.log({success , mistakes});
    changeResult (success)
}

function renderButton (parent) {
    let button = document.createElement('button');
    button.textContent = 'Готово';
    // Создаем слушателя события клик на кнопке
    button.addEventListener('click', () => onButtonClick(parent))
    parent.append(button);
}

function renderQuestions(questions) {
    // Получаем родительский элемент для всех вопросов
    let questionsElem = document.querySelector('#question')
    // Проходим по всем вопросам
    for (let i = 0; i < questions.length; i++) {
        let question = questions[i];
        let p = document.createElement('p');
        // Отрисовываем вопрос
        renderLabel (p, question.question, true)
        // Отрисовываем картинку
        renderImg (p, question.pathToImage,)
        // Проходим по всем ответам
        for (let j = 0; j < question.answers.length; j++) {
            let answer = question.answers[j];
            // Отрисовываем  чекбокс и запоминаем координату
            renderCheckBox (p, i, j)
            // Отрисовываем вариант ответа
            renderLabel (p, answer, false)

        }

        questionsElem.append(p);

    }
    // Отрисовываем кнопку для заверщения теста
    renderButton (questionsElem);
    renderResult (questionsElem);

}
